.. UML documentation master file, created by
   sphinx-quickstart on Mon Aug 12 08:21:25 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Unified Modeling Language
=========================

.. toctree::
   :maxdepth: 1
   :caption: Inhalt:
   :glob:

   stationen/*/*

   tools
