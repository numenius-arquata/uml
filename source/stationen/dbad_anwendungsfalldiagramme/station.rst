=======================
Anwendungsfalldiagramme
=======================


.. index:: UML; Anwendungsfalldiagramme

Anwendungsfalldiagramme, auch use-case-Diagramme genannt, zeigen das zu
modellierende System aus der Sicht der Anwender.

Es werden die Grenzen festgelegt und die mit dem System agierenden
Akteure. Es sind oft Personen in bestimmten Rollen, es kann sich aber
auch um bestimmte Programme und Komponenten handeln.

Aufgaben
--------

Erstellen Sie ein Anwendungsfalldiagramm für einen Aufgabenbereich in
Ihrer Firma, in dem Sie aktiv sind.

Symbol: Grenzen des Systems
---------------------------

- Die Grenzen werden durch ein Rechteck dargestellt.

- Die Akteure werden außerhalb der Systemgrenzen platziert, die
  Anwendungsfälle im System.

- Verbindungen zwischen Akteuren und System werden mit einfachen Linien
  dargestellt.

Symbol: Akteur
--------------

.. image:: ./images/akteur.svg

Alternative Darstellung
~~~~~~~~~~~~~~~~~~~~~~~

.. image:: ./images/akteur2.png

Symbol: Anwendungsfall
----------------------

.. image:: ./images/anwendungsfall.svg

Beispiel: Generalisierung
-------------------------

.. image:: ./images/generalisierung.svg

Beispiel: Reklamation
---------------------

.. image:: ./images/reklamation.svg

Beispiel:Kardinalität
---------------------

.. image:: ./images/kardinalitaet.svg

Beispiel: include
-----------------

.. image:: ./images/use-case-include.svg

Beispiel: extend
----------------

.. image:: ./images/use-case-extend.svg

Beispiel: Anwendungsfälle für »Hasse-Diagramme«
-----------------------------------------------

.. image:: ./images/mindset-usecase.svg

Denkpause:
----------


.. image:: images/schild-wuensch-dir-was.jpg
   :width: 0px

.. raw:: html
       
       <div style='text-align:center;'>
           <img src='../../_images/schild-wuensch-dir-was.jpg' 
                alt='Schild: Wünsch Dir was' width='400px' />
       </div>
