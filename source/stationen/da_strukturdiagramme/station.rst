=============================
Strukturdiagramme (Übersicht)
=============================


.. index:: UML; Strukturdiagramme (Übersicht)

Übersicht
---------

- Klassendiagramme

- Objektdiagramme

- Paketdiagramme

- Verteilungsdiagramme

- Komponentendiagramme

- Kompositionsstrukturdiagramm


.. image:: ./images/strukturdiagramme.png

Denkpause:
----------


.. image:: images/skulptur-schweriner-museum.jpg
   :width: 0px

.. raw:: html
       
       <div style='text-align:center;'>
           <img src='../../_images/skulptur-schweriner-museum.jpg' 
                alt='Schweriner Schloss' width='400px' />
	<p>Schwerin: Skulptur im Museum</p>
       </div>
