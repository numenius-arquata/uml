===================
Aktivitätsdiagramme
===================

.. index:: UML; Aktivitätsdiagramme

- zeigen die Reihenfolge der Abfolge von Aktionen

- enthalten auch Objekte, die im Zusammenhang mit der Aktion direkt auftreten

Aufgaben
--------

Zeichnen Sie ein Aktivitätsdiagramm für die Rückgabe von Leergut an einem Automaten.

Beispiel: Start einer Aktivität
-------------------------------

.. image:: ./files/ad-01.svg

Beispiel: Ein Objekt zwischen zwei Aktionen
-------------------------------------------

.. image:: ./files/ad-02.svg

Am Ausgang und am Eingang einer jeden Aktion wird das gleiche Objekt benutzt.   

Beispiel: unterschiedliche Objekte am Ein- bzw. Ausgang
-------------------------------------------------------

.. image:: ./files/ad-03.svg

Über einen Pin kann zusätzlich der Ausgang bzw. Eingabeparamter
benannt werden (hier nicht gezeigt).            

Beispiel: Aktivitätsende
------------------------

.. image:: ./files/ad-04.svg

Beispiel: Datenflußende
-----------------------

.. image:: ./files/ad-05.svg

Beispiel: Entscheidung und Zusammenführung
------------------------------------------

.. image:: ./files/ad-06.svg

Beispiel: Splitting und Syncronisation
--------------------------------------

.. image:: ./files/ad-07.svg

Beispiel: Wiederholung
----------------------

.. image:: ./files/ad-08.svg

Beispiel: Ticketsystem "Trac"
-----------------------------

.. image:: ./files/ad-09.svg


Beispiel: Spagetti kochen
-------------------------

.. image:: ./files/spagetti-kochen.svg


Denkpause:
----------


.. image:: files/sonderzug-2019.jpg
   :width: 0px

.. raw:: html
       
       <div style='text-align:center;'>
           <img src='../../_images/sonderzug-2019.jpg' 
                alt='Sonderzug 2019 Michendorf' width='400px' />
	<p>Sonderzug in Michendorf anno 2019</p>
       </div>
