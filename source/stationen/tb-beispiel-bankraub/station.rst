.. _bankraub:

==================
Beispiel: Bankraub
==================


.. index:: UML; Beispiel Bankraub

.. index:: Squenzdiagramm; Bankraub

.. index:: Bankraub; Squenzdiagramm


Für einen Bankraub wurde der Ablauf als Sequenzdiagram modelliert. 
Diese Szenarien sind natürlich nicht für den praktischen Einsatz
gedacht. Die Aufgabe diente lediglich als Übung für die Modellierung. 
Für alle angehenden Krimiautoren fünf interessante Vorlagen, inklusive
einem Augenzwinkern.

Viel Spaß beim vergleichen, betrachten, weiter fantasieren....

Version I
---------

.. image:: ./images/bankraub01.svg

Version II
----------

.. image:: ./images/bankraub02.svg

Version III
-----------

.. image:: ./images/bankraub03.svg

Version IV
----------

.. image:: ./images/bankraub04.png

Version V
---------

.. image:: ./images/bankraub05.png

Denkpause und Abkühlung nach diesen harten Fällen :-)
-----------------------------------------------------


.. image:: images/muehlstein-im-winter2020.webp
   :width: 0px

.. raw:: html
       
       <div style='text-align:center;'>
           <img src='../../_images/muehlstein-im-winter2020.webp' 
                alt='Mühlstein im Winter 2020' />
       </div>
