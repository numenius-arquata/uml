=============
Zeitdiagramme
=============


.. index:: UML; Zeitdiagramme

.. index:: Beispiele; Webbrowser (Zeitdiagramm)

.. index:: Webbrowser (Zeitdiagramm); Beispiele

Zeitdiagramme (timing diagram) zeigen den Ablauf einer Interaktion und
können Zustandsübergänge genau darstellen.

Aufgaben
--------

Suchen sie im Internet weitere Beispiele zu Zeitdiagrammen.

Beispiel 1 (Webbrowser)
-----------------------

.. index:: Beispiele; Webbrowser (Zeitdiagramm)

.. index:: Webbrowser (Zeitdiagramm); Beispiele

.. image:: ./files/timline.svg

.. raw:: html



    <div>
    Source Plantuml: <input type="checkbox" value="hint" />
    <div class="togglehint">
    <pre>
    // java -jar plantuml.jar -t svg quelle.txt

    @startuml
    robust "Web Browser" as WB
    concise "Web User" as WU
    @0
      WU is Idle
      WB is Idle
    @100
      WU is Waiting
      WB is Processing
    @300
      WB is Waiting
    @enduml
    </pre>
    </div>
    </div>

Beispiel 2 (Altersstufen)
-------------------------

.. index:: Beispiele; Alterstufen (Zeitdiagramm)

.. index:: Alterstufen (Zeitdiagramm); Beispiele


.. image:: ./files/altersstufen.svg

.. raw:: html



    <div>
    Source Plantuml: <input type="checkbox" value="hint" />
    <div class="togglehint">
    <pre>
    @startuml
    hide time-axis
    Title Das Altern
    robust "Altersstufen" as WB
    @0
    WB is Neugeborenes : 0..28 Tage
    @+100
    WB is Säugling : 1..12 Monate
    @+100
    WB is Kleinkind : 1..2 Jahre
    @+100
    WB is Spielealter : 6..11 Jahre
    @+100
    WB is "mittleres Alter" : 6..11 Jahre
    @+100
    WB is "Pubertät" : 12..18 Jahre
    @+100
    WB is "junger Erwachsener" : 19..30 Jahre
    @+100
    WB is "mittlerer Erwachsener" : 31..60 Jahre
    @+100
    WB is "Rentenalter" : 61..\* Jahre
    @+100
    WB is "Tod" : unvorhersehbar
    @+150
    @enduml

    </pre>
    </div>
    </div>

Denkpause:
----------


.. image:: files/schwerin-so-bin-ich.jpg
   :width: 0px

.. raw:: html
       
       <div style='text-align:center;'>
           <img src='../../_images/schwerin-so-bin-ich.jpg' 
                alt='Schwerin: So bin ich' width='400px' />
	<p>Schwerin Museum: Kunstwerk</p>
       </div>
