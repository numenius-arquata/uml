===============================
Verhaltensdiagramme (Übersicht)
===============================


.. index:: UML; Verhaltensdiagramme (Übersicht)

Übersicht
---------

Die folgenden Diagramme stellen das Modell in einem zeitlichen Kontext
dar.

Aufgaben
~~~~~~~~

Suchen Sie im Internet nach weiteren Beispielen für die auf den
folgenden Seiten dargestellten Verhaltensdiagramme.

Liste der Verhaltensdiagramme
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- Aktivitätsdiagramm

- Sequenzdiagramm

- Kommunikationsdiagramm

- Interaktionsübersichtsdiagramm

- Zeitverlaufsdiagramm

- Anwendungsfalldiagramm

- Zustandsdiagramm

.. image:: ./images/strukturdiagramme.png

Denkpause:
~~~~~~~~~~


.. image:: images/vertumnus-und-pomona2.jpg
   :width: 0px

.. raw:: html
       
       <div style='text-align:center;'>
           <img src='../../_images/vertumnus-und-pomona2.jpg' 
                alt='Schweriner Schloss' width='400px' />
        <p>Schwerin Gemälde: »Palastterrasse mit Vertumnus und Pomona« Peeter Gijsels (1621-1690)</p>
       </div>
