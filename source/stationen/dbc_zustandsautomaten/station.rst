===============
Zustandsautomat
===============


.. index:: UML; Zustandsautomat

Ein Zustandsautomat /(finite statemachine) /besteht aus Zuständen und
Zustandsübergängen (Transitionen). Weil das Modell eine
endlichen Anzahl von Zuständen beschreibt, wird es auch als
Finite-State-Maschine (FSM) bezeichnet.

- Ein Zustand ist eine Zeitspanne, in der ein Objekt auf ein Ereignis
  wartet.

- Ein Ereignis tritt immer zu einem Zeitpunkt auf und besitzt keine
  Dauer.

Im Unterschied zum Zustandsautomaten stehen beim Aktivitätsdiagramm die
Datenflüsse im Vordergrund.

.. image:: ./files/fza-lichtschalter.svg

.. raw:: html



    <div>
    Source Plantuml: <input type="checkbox" value="hint" />
    <div class="togglehint">
    <pre>

    // java -jar plantuml.jar -t svg quelle.txt

    @startuml
    state Lichtschalter{
    [\*] --> State1
    State1 :  Licht aus 
    State1 -right-> State2 : Kippschalter hoch
    State2 -> State1 : Kippschalter runter
    State2 :  Licht an 
    }
    @enduml

    </pre>
    </div>
    </div>

Wer sich intensiver mit Zustandsautomaten auseinandersetzen möchte,
kann die folgende Software ausprobieren:

`https://www.itemis.com/yakindu-werkzeuge <https://www.itemis.com/yakindu-werkzeuge>`_

Denkpause:
----------


.. image:: files/baumrinde-mit-flechten-rot.jpg
   :width: 0pxg

.. raw:: html
       
       <div style='text-align:center;'>
           <img src='../../_images/baumrinde-mit-flechten-rot.jpg' 
                alt='Baumrinde mit Flechten befallen' width='400px' />
       </div>
