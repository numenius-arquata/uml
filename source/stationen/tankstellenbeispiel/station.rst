====================
Beispiel: Tankstelle
====================


.. index:: UML; Beispiel Tankstelle

Für eine Tankstelle wurden alle Anwendungsfälle modelliert.  
Ein Vergleich aller Lösungsansätze wird hier exemplarisch 
gezeigt ohne eine Wertung vorzunehmen. Es wurde eine Skizze
angefertigt und der Entwurf dann mit einem UML-Tool digitalisiert.

Lösung 1
--------

.. image:: ./images/tank01.png

.. image:: ./images/tank02.png

Lösung 2
--------

.. image:: ./images/tank03.png

.. image:: ./images/tank04.png

Lösung 3
--------

.. image:: ./images/tank05.png

.. image:: ./images/tank06.png

Lösung 4
--------

.. image:: ./images/tank07.png

.. image:: ./images/tank08.png

Denkpause:
----------


.. image:: images/tintling.jpg
   :width: 0px

.. raw:: html
       
       <div style='text-align:center;'>
           <img src='../../_images/tintling.jpg' 
                alt='Pilze: Tintlinge' width='400px' />
       </div>
