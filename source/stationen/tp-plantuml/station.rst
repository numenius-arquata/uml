========
Plantuml
========


.. index:: Plantuml; lokal nutzen

.. index:: lokal nutzen; Plantuml


Wenn Sie mit »Plantuml« Diagramme auf dem eigenen Rechener erstellen
wollen, ist das auch möglich. Was zu beachten ist, wird hier gezeigt.

Download
--------

die jar-Datei herunterladen von: `https://plantuml.com/download <https://plantuml.com/download>`_

Grafik speichern
----------------

Aus dem Quellcode kann dann entweder eine Bilddatei im png-Format
bzw. eine SVG-Grafik erzeugt werden.

In einer Textdatei wird gespeichert was Online auf der Website auch
möglich ist. Ein Beispiel von der Website:

.. code-block:: bash


    @startuml
    Bob -> Alice : hello
    @enduml

Übersetzung
-----------

Wenn der Dreizeiler in /mein-diagramm.txt gespeichert wird, lautet
der Aufruf:

.. code-block:: bash


    java -jar plantuml.jar -tpng mein-diagramm.txt

Wenne es kein png-Bild, sondern eine svg-Grafik werden soll, dann:

.. code-block:: bash


    java -jar plantuml.jar -tsvg mein-diagramm.txt

Das Ergebins
------------

.. image:: ./images/mein-diagramm.svg
