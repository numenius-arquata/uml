===============
Sequenzdiagramm
===============


.. index:: UML; Sequenzdiagramm

Sequenzdiagramme zeigen ausgewählte Objekte und deren Interaktionen in
zeitlicher Reihenfolge.

Aufgaben
--------

- Fertigen Sie ein Sequenzdiagramm für die Zubereitung von Kaffee an.

- Wählen sie Ein Kriesenszenario (Überschwemmung,
  Asteroideneinschlag auf der Erde,...) und definieren Sie
  Meldeketten und Aktionen

Die :ref:`y»Bankraub« <bankraub>`-Übung zeigt Sequenzdiagramme
(inklusive Fehler).

Beispiel: Objekte und Lebenslinien
----------------------------------

.. index:: Stereotype

.. image:: ./files/seq02.svg

.. raw:: html



    <div>
    Plantuml Quellcode: <input type="checkbox" value="hint" />
    <div class="togglehint">
    <pre>
      // java -jar plantuml.jar -t svg 

    @startuml

    hide footbox

      participant participant as Foo
      actor       actor       as Foo1
      boundary    boundary    as Foo2
      control     control     as Foo3
      entity      entity      as Foo4
      database    database    as Foo5
      collections collections as Foo6
      queue       queue       as Foo7
      @enduml
    </pre>
    </div>
    </div>

Beispiel: Nachrichten
---------------------

.. index:: Beispiele; Sender & Empfänger  (Sequenzdiagramm)

.. index:: Sender & Empfänger (Sequenzdiagramm); Beispiele


Nachrichten (Messages) können mit und ohne Parameter/Rückgabewerte eingetragen
werden.

.. image:: ./files/seq03.svg

.. raw:: html



    <div>
    Plantuml Quellcode: <input type="checkbox" value="hint" />
    <div class="togglehint">
    <pre>

    // java -jar plantuml.jar -t svg quelle.txt

    @startuml

    participant Sender as S
    participant Empfänger as T

    S -> T: return message(parameter)
    activate S
    activate T
    deactivate T
    S -> T: message(parameter)
    activate T
    S <-- T: message(return)
    @enduml

    </pre>
    </div>
    </div>

Beispiel: Rekursion
-------------------

.. index:: Rekursion (Sequenzdiagramm)

.. index:: Sequenzdiagramm; Rekusion

Rekursionen und wiederholter Aufruf von Member-Funktionen sind
möglich.

.. image:: ./files/seq04.svg

.. raw:: html



    <div>
    Source Plantuml: <input type="checkbox" value="hint" />
    <div class="togglehint">
    <pre>

    // java -jar plantuml.jar -t svg quelle.txt

    @startuml
    [-> A: DoWork
    activate A
    A -> A: Internal call
    activate A
    A ->] : »createRequest«  
    A<--] : RequestCreated
    deactivate A
    [<- A: Done
    deactivate A
    @enduml

    </pre>
    </div>
    </div>

Beispiel: Ende einer Lebenslinie
--------------------------------

.. index:: Beispiele; Tournier (Sequenzdiagramm)

.. index:: Tournier (Sequenzdiagramm); Beispiele

.. index:: Lebenslinie; Existenzende (Sequenzdiagramm)

.. index:: Existenzende (Sequenzdiagramm); Lebenslinie


Wenn die Existenz eines Objektes beendet wird, zeigt ein Kreuz das Ende der
Lebenslinie an.

.. image:: ./files/seq05.svg


.. raw:: html



    <div>
    Source Plantuml: <input type="checkbox" value="hint" />
    <div class="togglehint">
    <pre>
    // java -jar plantuml.jar -t svg quelle.txt

    @startuml

    box "Schach"
    participant Tournier as T
    participant Organisation as O
    T -> O
    O -> game \*\* : Start
    activate game
    O <- game: End
    deactivate game
    O -> T !! : Ende
    endbox
    @enduml

    </pre>
    </div>
    </div>




.. image:: files/vertumnus-und-pomona3.jpg
   :width: 0px

.. raw:: html
       
       <div style='text-align:center;'>
           <img src='../../_images/vertumnus-und-pomona3.jpg' 
                alt='Schweriner Schloss' width='400px' />
	<p>Schwerin Gemälde: »Palastterrasse mit Vertumnus und Pomona« Peeter Gijsels (1621-1690)</p>
       </div>
