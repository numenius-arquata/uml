================
Klassendiagramme
================


.. index:: Klassendiagramme

Aufgaben
--------

- Erstellen Sie ein Klassendiagramm für eine Fläche, einen Kreis und ein
  Rechteck.

- Was ist in einem Haus eine Komposition , was eine Aggregation?

- Zeichen Sie ein Klassendiagramm für ein Haus.

Beispiel: Klasse String
-----------------------

.. index:: Klasse

.. image:: ./images/klasse.svg

Eine Klasse erhält einen Namen, Attribute und Methoden inclusive
Rückgabewerte.

Beispiel: Interface
-------------------

.. index:: Interface

.. image:: ./images/interface.svg


Ein Interface ist ein Vertrag. Für die Klasse müssen alle im Interface
deklarierten Methoden implementiert werden.

Beispiel: Interface abstrakt
----------------------------

.. index:: Interface abstrakt

.. image:: ./images/interface2.svg

Ein Interface kann durch einen Kreis auch abstrakt dargestellt werden.

Beispiel: Beziehung (Assoziation)
---------------------------------

.. index:: Assoziation

.. image:: ./images/assoziation.svg


Die Kardinalität (Multiplizität) kann an den Enden einer Beziehungslinie
angebracht werden.

Beispiel: Vererbung (Generalisation)
------------------------------------

.. index:: Vererbung

.. image:: ./images/generalisation.svg

Kreis erbt alle Eigenschaften von Flaeche.

Beispiel: Aggregation
---------------------

.. index:: Aggregation

.. image:: ./images/aggregation.svg

Adressbuch ist ein Aggregat. Es besteht aus Adressgruppen und Adressen
(Komposition).

Verschwindet das Adressbuch, sind auch keine Adressen Adressgruppen mehr
vorhandnen.

Wird eine Gruppe aus dem Adressbuch entfernt, existieren die Adressen
weiter (Aggregation).

Eine Komposition (Raute mit Füllung) beinhaltet immer eine existentielle
Abhängigkeit. Verschwindet das Aggregat einer Komposition, verschwinden
auch seine Teile. In einer Aggregation (Raute ohne Füllung) können die
Teile auch selbständig weiterexistieren.

Denkpause:
----------


.. image:: images/utensilien-an-der-wand.jpg
   :width: 0px

.. raw:: html
       
       <div style='text-align:center;'>
           <img src='../../_images/utensilien-an-der-wand.jpg' 
                alt='Schweriner Schloss' width='400px' />
	<p>Schwerin: Gemälde im Museum</p>
       </div>
