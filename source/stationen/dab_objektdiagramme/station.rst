================
Objekt-Diagramme
================


.. index:: UML; Objekt-Diagramme

Objektdiagramme sind Schnappschüsse von Klassendiagrammen, mit
konkreten Werten zu einem definierten Zeitpunkt.

Aufgabe
-------

Erstellen Sie ein Objektdiagramm für eine Fläche, einen Kreis und ein
Rechteck.

Beispiel: Klasse und Objekt
---------------------------

.. image:: ./images/class-object-angestellter.svg


.. table::

    +--------------+---------------------------+---------------------------------------------------------------------+
    | Anmerkung    | Klasse                    | Objekt                                                              |
    +==============+===========================+=====================================================================+
    | Benennung    | Klassenname               | Objektname, Doppelpunkt, Klassenname                                |
    +--------------+---------------------------+---------------------------------------------------------------------+
    | Besonderheit | keine Unterstreichung     | Untersteichung für Objeket und Klasse (im Beispiel nicht umgesetzt) |
    +--------------+---------------------------+---------------------------------------------------------------------+
    | Werte        | Attribute mit Typ-Angaben | Attribute mit konkreten Werten                                      |
    +--------------+---------------------------+---------------------------------------------------------------------+

Beispiel: Klassen Auto/Rad
--------------------------

.. image:: ./images/auto-rad-klassen.svg

Objekt: Bugatti/Räder
---------------------

.. image:: ./images/auto-rad-objekte.svg

Denkpause:
----------

Ein Butatti, damals...
~~~~~~~~~~~~~~~~~~~~~~

.. image:: ./images/bugattibig.jpg

... und später...
~~~~~~~~~~~~~~~~~

.. image:: ./images/bugatti-new.jpg
