========================
UML: Geschichte/Struktur
========================


.. index:: UML; Geschichte

Handlungsanweisungen/Fragen
---------------------------

Zu welcher Firma gehörten die Begründer der UML-Syntax?

Wird der UML-Standard vom W3C gepflegt?                                                                                       

Finden Sie weitere Tutorials zum Thema UML.               

Neue Version [1]_
~~~~~~~~~~~~~~~~~

Als Erfinder gelten die 3 Amigos: Grady Booch, James Rumbaugh, und Ivar Jacobson.
Wie so oft ist es aber Ergebnis einer Entwicklung, wie in folgender Darstellung zu sehen ist.

.. image:: ./images/oo-history.png


.. image:: ./images/uml-history.png


.. table::

    +------------------+------+---------------+
    | Standard         | 1997 | erste Version |
    +------------------+------+---------------+
    | aktuelle Version | 2017 | UML 2.5.1.    |
    +------------------+------+---------------+


.. index:: UML; Gruppierung der Diagammtypen

Die Version 2 umfasst dreizehn Diagrammtypen für die graphische Modellierung von Systemen.                          
Sie teilt die Diagrammtypen in zwei Gruppen ein: **Strukturdiagramme** und **Verhaltensdiagramme**.                     

Strukturdiagramme
^^^^^^^^^^^^^^^^^

Strukturdiagramme zeigen Sichten auf die statische Struktur eines Systems.

• Klassendiagramm
• Kompositionsstrukturdiagramm
• Komponentendiagramm
• Verteilungsdiagramm
• Objektdiagramm 
• Paketdiagramm

Verhaltensdiagramme
^^^^^^^^^^^^^^^^^^^

Verhaltensdiagramme beziehen auch die zeitliche Dimension, vor allem das Nacheinander in die Darstellung mit ein.

• Aktivitätsdiagramm
• Sequenzdiagramm
• Kommunikationsdiagramm 
• Interaktionsübersichtsdiagramm
• Zeitverlaufsdiagramm
• Anwendungsfalldiagramm
• Zustandsdiagramm

Denkpause:
----------


.. image:: images/skulptur-loewe.jpg
   :width: 0px

.. raw:: html
       
       <div style='text-align:center;'>
           <img src='../../_images/skulptur-loewe.jpg' 
                alt='Schweriner Schloss' width='400px' />
	<p>Skulptur an einer Kirche in Schwerin</p>
       </div>


.. [1] `http://de.wikipedia.org/wiki/Unified_Modeling_Language <http://de.wikipedia.org/wiki/Unified_Modeling_Language>`_                                                          
