=============
UML-Werkzeuge
=============


.. index:: Onlinewerkzeuge

.. index:: Tools; Online

Linkliste
---------

Plantuml
~~~~~~~~

- Erklärungen: `http://plantuml.com/ <http://plantuml.com/>`_

- Testserver: `http://www.plantuml.com/plantuml/uml <http://www.plantuml.com/plantuml/uml>`_

- Plantuml + Sphinx: `https://pypi.org/project/sphinxcontrib-plantuml/ <https://pypi.org/project/sphinxcontrib-plantuml/>`_

- Eigenen Testserver aufsetzen (Docker und andere Installationsanleitungen):

  `https://docs.gitlab.com/ee/administration/integration/plantuml.html <https://docs.gitlab.com/ee/administration/integration/plantuml.html>`_

- Download des jar-Files für den lokalen Einsatz

  `https://plantuml.com/download <https://plantuml.com/download>`_

- Dokumentation in verschienden Sprachen

  `http://plantuml.com/de/guide <http://plantuml.com/de/guide>`_

UMLetino
~~~~~~~~

- `http://www.umletino.com <http://www.umletino.com>`_

Domain Driven Design
~~~~~~~~~~~~~~~~~~~~

- `https://www.wps.de/modeler/ <https://www.wps.de/modeler/>`_

Echtzeitvisualisierung
~~~~~~~~~~~~~~~~~~~~~~

- `Simlanes, angelehnt an Sequenzdiagramme <https://swimlanes.io/>`_

- `Flowcharts, angelehnt an Anwendungsfalldiagramme <https://flowchart.fun/>`_
